const Course = require("../Models/coursesSchema.js");
const auth = require("../auth.js");

// Create a new course
/*
	Steps:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new User to the database
*/

module.exports.addCourse = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin === false){
		return response.send("Not an admin. You are not allowed to create a course!")
	}else{
		let input = request.body;

		let newCourse = new Course({
			name: input.name,
			description: input.description,
			price: input.price
		});

		// saves the created object to our database
		return newCourse.save()
		// course successfully created
		.then(course => {
			response.send("Course added successfully!");
		})
		// course creation failed
		.catch(error => {
			response.send(error);
		})
	}
}

// Create a controller where in it will retrieve all the courses (active/inactive)

module.exports.allCourses = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	if(!userData.isAdmin) {
		return response.send("You don't have access to this route!");
	}else{
		Course.find({}).then(result => response.send(result))
		.catch(error => response.send(error));
	}
}

// Create a controller where in it will retrieve courses that are active.

module.exports.allActiveCourses = (request, response) => {
	Course.find({isActive: true})
	.then(result => response.send(result))
	.catch(error => response.send(error));
}

/*
	Mini Activity:
		1. You are going to create a route wherein it can retrieve all inactive courses.
		2. Make sure that the admin users only are the ones that can acess this route.
*/

module.exports.allInactiveCourses = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	if(!userData.isAdmin) {
		return response.send("You don't have access to this route!");
	}else{
		Course.find({isActive: false})
		.then(result => response.send(result))
		.catch(error => response.send(error));
	}
}

// This controller will get the details of specific course

module.exports.courseDetails = (request, response) => {
	// to get the params from the url
	const courseId = request.params.courseId;
	Course.findById(courseId)
	.then(result => response.send(result))
	.catch(error => response.send(error))
}

// This controller is for updating specific course
/*
	Business logic:
		1. We are going to edit/update the course that is stored in the params.
*/

module.exports.updateCourse = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const courseId = request.params.courseId;
	const input = request.body;
	if(!userData.isAdmin){
		return response.send("You don't have access in this page!")
	}else{
		Course.findById(courseId)
		.then(result => {
			if(result === null){
				return response.send("courseId is invalid, please try again!")
			}else{
				let updatedCourse = {
					name: input.name,
					description: input.description,
					price: input.price
				}
				Course.findByIdAndUpdate(courseId, updatedCourse, {new: true})
				.then(result => {
					return response.send(result)})
				.catch(error => response.send(error))
			}
		})
		.catch(error => response.send(error))
	}
}

// [Activity]
module.exports.archiveCourse = (request, response) => {
	const userData = auth.decode(request.headers.authorization);
	const courseId = request.params.courseId;
	const input = request.body;
	if(!userData.isAdmin){
		return response.send("You don't have access in this page!")
	}else{
		let archiveCourse = {
			isActive: input.isActive
		}
		Course.findByIdAndUpdate(courseId, archiveCourse, {new: true})
		.then(result => response.send(true))
		.catch(error => response.send(error))
	}
}