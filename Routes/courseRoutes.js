const express = require("express");
const router = express.Router();
const auth = require("../auth.js");
const courseController = require("../Controllers/courseController");

// [Routes without params]
// Route for creating a course
router.post("/", auth.verify, courseController.addCourse);

// Route for retrieveing all courses
router.get("/all", auth.verify, courseController.allCourses);

// Route for retrieving all active courses
router.get("/allActive", courseController.allActiveCourses);

// Route for retrieving all inactive courses
router.get("/allInactive", auth.verify, courseController.allInactiveCourses);

// [Routes with params]
// Route for retrieving detail/s of specific course
router.get("/:courseId", courseController.courseDetails);

// Route for updating detail/s of specific course
router.put("/update/:courseId", auth.verify, courseController.updateCourse);

// Route for archiving a course
router.put("/archive/:courseId", auth.verify, courseController.archiveCourse);

module.exports = router;